---
title: "Plasma Style Tutorial"
weight: 3
description: >
  Learn how to create a KDE Plasma Style.
aliases:
  - /docs/plasma/theme/
---
