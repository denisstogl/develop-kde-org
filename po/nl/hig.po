#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: documentation-develop-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:19+0000\n"
"PO-Revision-Date: 2023-01-17 11:11+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#: content/hig/_index.md:0
msgid "KDE Human Interface Guidelines"
msgstr "Human-interface richtlijnen van KDE"

#: content/hig/_index.md:9
msgid ""
"The **KDE Human Interface Guidelines (HIG)** offer designers and developers "
"a set of recommendations for producing beautiful, usable, and consistent "
"user interfaces for convergent desktop and mobile applications and workspace "
"widgets. Our aim is to improve the experience for users by making consistent "
"interfaces for desktop, mobile, and everything in between, more consistent, "
"intuitive and learnable."
msgstr ""
"De **Human-interface richtlijnen (HIG) van KDE** bieden ontwerpers en "
"ontwikkelaars een set aanbevelingen voor het produceren van mooie, bruikbare "
"en consistente gebruikersinterfaces voor convergente bureaubladen en mobiele "
"toepassingen en widgets in werkruimten. Ons doel is om de ervaringen voor "
"gebruikers te verbeteren door consistente interfaces voor het bureaublad te "
"maken en alles er tussenin, consistenter, intuïtiever en te leren."

#: content/hig/_index.md:15
msgid "Design Vision"
msgstr "Visie op ontwerp"

#: content/hig/_index.md:18
msgid ""
"Our design vision focuses on two attributes of KDE software that connect its "
"future to its history:"
msgstr ""
"Onze visie op ontwerp focust op twee attributen van KDE software die zijn "
"toekomst verbindt met zijn geschiedenis:"

#: content/hig/_index.md:21
msgid ""
"![Simple by default, powerful when needed.](/hig/HIGDesignVisionFullBleed."
"png)"
msgstr ""
"![Standaard eenvoudig, krachtig wanneer nodig.](/hig/"
"HIGDesignVisionFullBleed.png)"

#: content/hig/_index.md:23
msgid "Simple by default..."
msgstr "Standaard eenvoudig..."

#: content/hig/_index.md:25
msgid ""
"*Simple and inviting. KDE software is pleasant to experience and easy to use."
"*"
msgstr ""
"*Eenvoudig en uitnodigend. KDE software is plezierig om te ervaren en "
"gemakkelijk te gebruiken.*"

#: content/hig/_index.md:28
msgid "**Make it easy to focus on what matters**"
msgstr "**Maak het makkelijk om te focussen op wat belangrijk is**"

#: content/hig/_index.md:30
msgid ""
"Remove or minimize elements not crucial to the primary or main task. Use "
"spacing to keep things organized. Use color to draw attention. Reveal "
"additional information or optional functions only when needed."
msgstr ""
"Verwijder of minimaliseer elementen die niet cruciaal zijn voor de primaire "
"of hoofdtaak. Gebruik spatiëring om zaken georganiseerd te houden. Gebruik "
"kleur om aandacht te vragen. Laat extra informatie zien of optionele "
"functies alleen wanneer nodig."

#: content/hig/_index.md:35
msgid "**\"I know how to do that!\"**"
msgstr "**\"Ik weet hoe dat te doen!\"**"

#: content/hig/_index.md:37
msgid ""
"Make things easier to learn by reusing design patterns from other "
"applications. Other applications that use good design are a precedent to "
"follow."
msgstr ""
"Maak zaken gemakkelijker te leren door ontwerppatronen uit andere "
"toepassingen opnieuw te gebruiken. Andere toepassingen die een goed ontwerp "
"gebruiken zijn een precedent om te volgen."

#: content/hig/_index.md:41
msgid "**Do the heavy lifting for me**"
msgstr "**Doe het zware werk voor mij**"

#: content/hig/_index.md:43
msgid ""
"Make complex tasks simple. Make novices feel like experts. Create ways in "
"which your users can naturally feel empowered by your software."
msgstr ""
"Maak complexe taken eenvoudig. Maak dat beginnelingen zich experts voelen. "
"Maak manieren waarin uw gebruikers zich natuurlijk gesteund voelen door uw "
"software."

#: content/hig/_index.md:47
msgid "...Powerful when needed"
msgstr "...Krachtig wanneer nodig"

#: content/hig/_index.md:49
msgid ""
"*Power and flexibility. KDE software allows users to be effortlessly "
"creative and efficiently productive.*"
msgstr ""
"*Krachtig en flexibel. KDE software biedt gebruikers om zonder inspanning "
"creatief te zijn en efficiënt productief.*"

#: content/hig/_index.md:52
msgid "**Solve a problem**"
msgstr "**Los een probleem op**"

#: content/hig/_index.md:54
msgid ""
"Identify and make very clear to the user what need is addressed and how."
msgstr ""
"Identificeer en maak erg helder aan de gebruiker welke behoefte wordt "
"bevredigt en hoe."

#: content/hig/_index.md:57
msgid "**Always in control**"
msgstr "**Altijd in controle**"

#: content/hig/_index.md:59
msgid ""
"It should always be clear what can be done, what is currently happening, and "
"what has just happened. The user should never feel at the mercy of the tool. "
"Give the user the final say."
msgstr ""
"Het zou altijd helder moeten zijn wat er gedaan kan worden, wat er nu "
"gebeurt en wat er zojuist is gebeurd. De gebruiker zou zich nooit aan de "
"genade van het hulpmiddel overgeleverd moeten voelen. Geef de gebruiker de "
"uiteindelijke beslissing."

#: content/hig/_index.md:64
msgid "**Be flexible**"
msgstr "**Wees flexibel**"

#: content/hig/_index.md:66
msgid ""
"Provide sensible defaults but consider optional functionality and "
"customization options that don\\'t interfere with the primary task."
msgstr ""
"Biedt zinvolle standaarden maar overweeg optionele opties voor "
"functionaliteit en aanpasbaarheid die niet interfereren met de primaire taak."

#: content/hig/_index.md:70
msgid "Note"
msgstr "Noot"

#: content/hig/_index.md:72
msgid ""
"KDE encourages developing and designing for customization, while providing "
"good default settings. Integrating into other desktop environments is also a "
"virtue, but ultimately we aim for perfection within our own Plasma desktop "
"environment with the default themes and settings. This aim should not be "
"compromised."
msgstr ""
"KDE bemoedigt ontwikkelen en ontwerpen voor aanpasbaarheid, terwijl ook "
"goede standaard instellingen worden geleverd. Integreren in andere "
"bureaubladomgevingen is ook een deugd, maar uiteindelijk richten we ons op "
"perfectie in onze eigen Plasma bureaubladomgeving met de standaard thema's "
"en instellingen. Aan dit doel zou niet getornd mogen worden."
